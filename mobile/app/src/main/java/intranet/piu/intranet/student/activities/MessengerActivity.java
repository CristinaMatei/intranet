package intranet.piu.intranet.student.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.adapters.MessengerAdapter;
import intranet.piu.intranet.student.models.Message;
import intranet.piu.intranet.student.models.Messenger;
import intranet.piu.intranet.student.models.Student;

public class MessengerActivity extends AppCompatActivity implements View.OnClickListener, MessengerAdapter.OnMessageClick {

    private RecyclerView mRecyclerView;
    private FloatingActionButton mFAB;
    private MessengerAdapter mAdapter;

    private int mLastPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);

        mRecyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new MessengerAdapter(Messenger.getMessages(), this);
        mRecyclerView.setAdapter(mAdapter);

        mFAB = findViewById(R.id.fab);
        mFAB.setOnClickListener(this);

        setTitle(R.string.messenger);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab) {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.layout_autocomplete_dialog);
            dialog.setTitle("New Message");

            final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) dialog.findViewById(R.id.tv_autocomplete);
            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.list_item_autocomplete, R.id.text, Student.getStudentsName());
            autoCompleteTextView.setAdapter(adapter);
            final EditText editText = (EditText) dialog.findViewById(R.id.et_message);

            Button sendButton = (Button) dialog.findViewById(R.id.button_send);
            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (autoCompleteTextView.getText() != null) {
                        final String student = autoCompleteTextView.getText().toString();
                        final String message = editText.getText().toString();
                        if (student.isEmpty() || message.isEmpty()) {
                            new AlertDialog.Builder(MessengerActivity.this)
                                    .setMessage("Error: empty fields!")
                                    .setPositiveButton("OK", null)
                                    .show();
                        } else if (!Student.containsStudentName(student)) {
                            new AlertDialog.Builder(MessengerActivity.this)
                                    .setMessage("Error: incorrect name!")
                                    .setPositiveButton("OK", null)
                                    .show();
                        } else {
                            new AlertDialog.Builder(MessengerActivity.this)
                                    .setMessage("Are you sure you want to send this message?")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Student s = Student.getStudent(student);
                                            if (Messenger.containsStudentName(student)) {
                                                Messenger.removeMessage(s);
                                            }
                                            Calendar calendar = Calendar.getInstance();
                                            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
                                            int minutesOfday = calendar.get(Calendar.MINUTE);
                                            Message m = new Message(message, hourOfDay + ":" + minutesOfday);
                                            Messenger.getMessages().add(0, new Messenger(s, m));
                                            mAdapter.setItems(Messenger.getMessages());
                                            Message.getChat().add(m);
                                        }
                                    })
                                    .setNegativeButton("No", null)
                                    .show();

                        }
                    } else {
                        new AlertDialog.Builder(MessengerActivity.this)
                                .setMessage("Error: empty fields!")
                                .setPositiveButton("OK", null)
                                .show();
                    }
                    dialog.dismiss();
                }
            });
            Button cancelButton = (Button) dialog.findViewById(R.id.button_cancel);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }


    @Override
    public void onMessageClicked(Messenger messenger, int position) {
        mLastPosition = position;
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.STUDENT_KEY, messenger.getStudent().getName());
        startActivity(intent);
    }
}
