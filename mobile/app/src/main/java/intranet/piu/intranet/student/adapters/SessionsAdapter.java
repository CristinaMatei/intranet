package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;

public class SessionsAdapter extends RecyclerView.Adapter<SessionsAdapter.SessionViewHolder> {

    class SessionViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {

        TextView mSession;

        public SessionViewHolder(View itemView) {
            super(itemView);
            mSession = (TextView) itemView.findViewById(R.id.session);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onSessionClick(mItems.get(position));
        }
    }

    public interface OnSessionClickListener {
        void onSessionClick(String session);
    }

    private List<String> mItems;
    private OnSessionClickListener mListener;

    public SessionsAdapter(List<String> mItems, OnSessionClickListener mListener) {
        this.mItems = mItems;
        this.mListener = mListener;
    }

    public void setmItems(List<String> mItems) {
        this.mItems = mItems;
        notifyDataSetChanged();
    }

    @Override
    public SessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.list_item_session, parent, false);
        return new SessionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SessionViewHolder holder, int position) {
        holder.mSession.setText(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }
}
