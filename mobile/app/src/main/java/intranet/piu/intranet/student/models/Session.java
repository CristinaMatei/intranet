package intranet.piu.intranet.student.models;

import java.util.ArrayList;
import java.util.List;


public class Session {

    public static String FIRST_YEAR = "1st Year";
    public static String SECOND_YEAR = "2nd Year";
    public static String THIRD_YEAR = "3rd Year";
    public static String CURRENT_SESSION = "Current session";

    public static List<String> getSessions() {
        List<String> list = new ArrayList<>();
        list.add(FIRST_YEAR);
        list.add(SECOND_YEAR);
        list.add(THIRD_YEAR);
        list.add(CURRENT_SESSION);
        return list;
    }
}
