package intranet.piu.intranet.student.models;


import java.util.ArrayList;
import java.util.List;

public class Grade {

    private static List<Grade> sCurrentGrades;

    public static List<Grade> getFisrtYearGrades() {
        List<Grade> list = new ArrayList<>();
        list.add(new Grade("Algebra liniara si geometrie analitica", 10));
        list.add(new Grade("Analiza matematica 1", 9));
        list.add(new Grade("Analiza si sinteza dispozitivelor numerice", 7));
        list.add(new Grade("Fizica", 10));
        list.add(new Grade("Limba straina 1", 9));
        list.add(new Grade("Matematici Speciale", 7));
        list.add(new Grade("Programarea calculatoarelor", 9));
        list.add(new Grade("Analiza matematica 2", 9));
        list.add(new Grade("Electrotehnica", 10));
        list.add(new Grade("Limba straina 2", 10));
        list.add(new Grade("Matematici speciale in inginerie", 10));
        list.add(new Grade("Proiectarea sistemelor numerice", 8));
        list.add(new Grade("Structuri de date si algoritmi", 9));
        return list;
    }

    public static List<Grade> getSecondYearGrades() {
        List<Grade> list = new ArrayList<>();
        list.add(new Grade("Baze de date", 9));
        list.add(new Grade("Calcul numeric", 9));
        list.add(new Grade("Circuite analogice si numerice", 10));
        list.add(new Grade("Lisba straina 1", 10));
        list.add(new Grade("Masurari electronice si senzori", 10));
        list.add(new Grade("Programare in limbaj de asamblare", 10));
        list.add(new Grade("Programare orientata pe obiect", 10));
        list.add(new Grade("Algoritmi fundamentali", 10));
        list.add(new Grade("Arhitectura calculatoarelor", 10));
        list.add(new Grade("Elemente de grafica asistata de calculator", 8));
        list.add(new Grade("Limba straina 2", 9));
        list.add(new Grade("Sisteme de operare", 9));
        list.add(new Grade("Tehnici de programare fundamentale", 10));
        list.add(new Grade("Teoria sistemelor", 10));
        return list;
    }

    public static List<Grade> getThirdYearGrades() {
        List<Grade> list = new ArrayList<>();
        list.add(new Grade("Inginerie software", 10));
        list.add(new Grade("Introducere in inteligenta artificiala", 10));
        list.add(new Grade("Legislatie economica", 10));
        list.add(new Grade("Programare functionala", 10));
        list.add(new Grade("Programare logica", 10));
        list.add(new Grade("Proiectare cu microprocesoare", 10));
        list.add(new Grade("Sisteme de prelucrare grafica", 8));
        list.add(new Grade("Limbaje formale si translatoare", 10));
        list.add(new Grade("Management si comunicare", 10));
        list.add(new Grade("Procesarea imaginilor", 10));
        list.add(new Grade("Proiectare software", 10));
        list.add(new Grade("Sisteme inteligente", 9));
        list.add(new Grade("Structura sistemelor de calcul", 9));
        return list;
    }


    static {
        sCurrentGrades = new ArrayList<>();
        sCurrentGrades.add(new Grade("Calcul paralel si distribuit"));
        sCurrentGrades.add(new Grade("Dezvoltare personala si profesionala", 10));
        sCurrentGrades.add(new Grade("Proiectarea interfetelor utilizator", 8, true, true));
        sCurrentGrades.add(new Grade("Proiectarea translatoarelor", 7, false));
        sCurrentGrades.add(new Grade("Retele de calculatoare"));
        sCurrentGrades.add(new Grade("Sisteme distribuite", 9, true));
    }

    public static List<Grade> getCurrentGrades() {
        return sCurrentGrades;
    }


    private String course;
    private int grade;
    private boolean ok;
    private boolean newGrade;

    public Grade(String course, int grade) {
        this.course = course;
        this.grade = grade;
        ok = true;
        newGrade = false;
    }

    public Grade(String course) {
        this.course = course;
        grade = 0;
        ok = true;
        newGrade = false;
    }

    public Grade(String course, int grade, boolean ok) {
        this.course = course;
        this.grade = grade;
        this.ok = ok;
        newGrade = false;
    }

    public Grade(String course, int grade, boolean ok, boolean newGrade) {
        this.course = course;
        this.grade = grade;
        this.ok = ok;
        this.newGrade = newGrade;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public boolean isNewGrade() {
        return newGrade;
    }

    public void setNewGrade(boolean newGrade) {
        this.newGrade = newGrade;
    }
}
