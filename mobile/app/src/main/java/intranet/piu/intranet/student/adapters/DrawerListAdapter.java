package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import intranet.piu.intranet.R;


public class DrawerListAdapter extends ArrayAdapter<String> {

    private String[] mItems;
    private Context mContext;

    public DrawerListAdapter(String[] items, Context context) {
        super(context, R.layout.drawer_item, items);
        mContext = context;
        mItems = items;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.drawer_item, viewGroup, false);
        }
        String item = getItem(i);
        ((TextView) view.findViewById(R.id.tv_drawer)).setText(item);
        ImageView imageView = view.findViewById(R.id.drawer_icon);
        if (mContext.getString(R.string.title_announcements).equals(item)) {
            imageView.setImageResource(R.drawable.ic_announcements);
        } else if (mContext.getString(R.string.title_grades).equals(item)) {
            imageView.setImageResource(R.drawable.ic_grades);
        } else if (mContext.getString(R.string.title_classes).equals(item)) {
            imageView.setImageResource(R.drawable.ic_classes);
        } else if (mContext.getString(R.string.title_schedule).equals(item)) {
            imageView.setImageResource(R.drawable.ic_schedule);
        } else if ("Group Mates".equals(item)) {
            imageView.setImageResource(R.drawable.ic_group);
        } else if ("Log Out".equals(item)) {
            imageView.setImageResource(R.drawable.ic_logout);
        }
        return view;
    }
}
