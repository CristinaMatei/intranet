package intranet.piu.intranet.teacher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import intranet.piu.intranet.LoginActivity;
import intranet.piu.intranet.R;
import intranet.piu.intranet.student.activities.GroupActivity;
import intranet.piu.intranet.student.activities.MessengerActivity;
import intranet.piu.intranet.student.adapters.DrawerListAdapter;
import intranet.piu.intranet.student.navigation_fragments.AnnouncementsFragment;
import intranet.piu.intranet.student.navigation_fragments.ScheduleFragment;
import intranet.piu.intranet.teacher.navigation_fragments.TeacherClassesFragment;

public class TeacherActivity extends AppCompatActivity {
    private static final int POSITION_ANNOUNCEMENTS = 0;
    private static final int POSITION_CLASSES = 1;
    private static final int POSITION_SCHEDULE = 2;
    private static final int POSITION_LOGOUT = 3;

    private Fragment mFragment;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ActionBar mActionBar;
    private Toolbar mToolbar;
    private ListView mDrawerList;
    BottomNavigationView mNavigation;

    boolean doubleBackToExitPressedOnce = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_classes:
                    mFragment = new TeacherClassesFragment();
                    setTitle(R.string.title_classes);
                    break;
                case R.id.navigation_schedule:
                    mFragment = new ScheduleFragment();
                    setTitle(R.string.title_schedule);
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_teacher, mFragment);
            transaction.commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);

        mNavigation = (BottomNavigationView) findViewById(R.id.navigation_t);
        mNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mFragment = new TeacherClassesFragment();
        setTitle(R.string.title_classes);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_teacher, mFragment);
        transaction.commit();

        setupActionBar();
        setupDrawer();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.student_action_menu, menu);
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        final SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        String dataArr[] = {"Classes", "Schedule"};
        ArrayAdapter<String> newsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, dataArr);
        searchAutoComplete.setAdapter(newsAdapter);
        searchAutoComplete.setTextColor(Color.WHITE);
        searchAutoComplete.setDropDownBackgroundResource(android.R.color.white);
        searchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemIndex, long id) {
                String queryString = (String) adapterView.getItemAtPosition(itemIndex);
                searchAutoComplete.setText(queryString);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconified(true);
                searchView.onActionViewCollapsed();
                search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search: {
                return true;
            }
            case R.id.action_messenger: {
                startActivity(new Intent(this, MessengerActivity.class));
                return true;
            }
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_t);
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeButtonEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit!", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    private void setupDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_teacher);
        mDrawerList = (ListView) findViewById(R.id.list_view);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                mDrawerLayout.bringToFront();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        String[] items = getResources().getStringArray(R.array.drawer_items_teacher);
        DrawerListAdapter adapter = new DrawerListAdapter(items, this);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case POSITION_ANNOUNCEMENTS: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mNavigation.setSelectedItemId(R.id.navigation_announcements);
                        break;
                    }
                    case POSITION_CLASSES: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mNavigation.setSelectedItemId(R.id.navigation_classes);
                        break;
                    }
                    case POSITION_SCHEDULE: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mNavigation.setSelectedItemId(R.id.navigation_schedule);
                        break;
                    }
                    case POSITION_LOGOUT: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        logOut();
                        break;
                    }
                }
            }
        });
    }

    private void logOut() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.key_username), "");
        editor.apply();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void search(String query) {
        if (getString(R.string.title_announcements).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_announcements);
        } else if (getString(R.string.title_grades).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_grades);
        } else if (getString(R.string.title_classes).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_classes);
        } else if (getString(R.string.title_schedule).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_schedule);
        } else if ("Group Mates".equals(query)) {
            startActivity(new Intent(this, GroupActivity.class));
        } else if ("Messenger".equals(query)) {
            startActivity(new Intent(this, MessengerActivity.class));
        }
    }
}