package intranet.piu.intranet.student.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.activities.GradesActivity;

public class NotificationUtils {

    public static void sendNotification(Context context, String message) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context);

        Intent intent = new Intent(context, GradesActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.drawable.ic_notification);
        mBuilder.setContentTitle("Intranet Notification");
        mBuilder.setContentText(message);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(001, mBuilder.build());
    }
}
