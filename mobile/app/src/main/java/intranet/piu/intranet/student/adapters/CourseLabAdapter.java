package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;

public class CourseLabAdapter extends BaseAdapter {

    private List<String> mItems;
    private Context mContext;

    public CourseLabAdapter(List<String> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }


    @Override
    public int getCount() {
        if (mItems != null)
            return mItems.size();
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (mItems != null)
            return mItems.get(i);
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = (view == null) ? inflater.inflate(R.layout.list_item_course_lab, viewGroup, false) : view;
        ((TextView) row.findViewById(R.id.file_name)).setText(mItems.get(i));
        return row;
    }
}
