package intranet.piu.intranet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import intranet.piu.intranet.student.StudentActivity;
import intranet.piu.intranet.teacher.TeacherActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static String STUDENT_USERNAME = "student";
    private static String TEACHER_USERNAME = "teacher";
    private static String PASSWORD = "0000";

    private EditText mUsername;
    private EditText mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsername = (EditText) findViewById(R.id.input_username);
        mPassword = (EditText) findViewById(R.id.input_password);
        findViewById(R.id.b_login).setOnClickListener(this);
        findViewById(R.id.tv_change_password).setOnClickListener(this);

        checkAuthentication();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_login: {
                String username = mUsername.getText().toString();
                String password = mPassword.getText().toString();
                if (validateForm(username, password)) {
                    if (STUDENT_USERNAME.equals(username)) {
                        if (PASSWORD.equals(password)) {
                            saveUsername(username);
                            startStudentActivity();
                        } else {
                            showDialog(getText(R.string.error_wrong_password).toString());
                        }
                    } else if (TEACHER_USERNAME.equals(username)) {
                        if (PASSWORD.equals(password)) {
                            saveUsername(username);
                            startTeacherActivity();
                        } else {
                            showDialog(getText(R.string.error_wrong_password).toString());
                        }
                    } else {
                        showDialog(getText(R.string.error_wrong_urs_pwd).toString());
                    }
                }
                break;
            }
            case R.id.tv_change_password: {
                showDialog(getText(R.string.message_reset_password).toString());
                break;
            }
        }
    }

    private boolean validateForm(String username, String password) {
        boolean valid = true;
        if (TextUtils.isEmpty(username)) {
            mUsername.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mUsername.setError(null);
        }
        if (TextUtils.isEmpty(password)) {
            mPassword.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mPassword.setError(null);
        }
        return valid;
    }

    private void showDialog(String message) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(LoginActivity.this);
        dlgAlert.setMessage(message);
        dlgAlert.setPositiveButton(getText(R.string.ok), null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    private void checkAuthentication() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String username = sharedPref.getString(getString(R.string.key_username), "");
        if (STUDENT_USERNAME.equals(username)) {
            startStudentActivity();
        } else if (TEACHER_USERNAME.equals(username)) {
            startTeacherActivity();
        }
    }

    private void saveUsername(String username) {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.key_username), username);
        editor.apply();
    }

    private void startStudentActivity() {
        Intent intent = new Intent(LoginActivity.this, StudentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void startTeacherActivity() {
        Intent intent = new Intent(LoginActivity.this, TeacherActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
