package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.models.Grade;

public class GradesAdapter extends RecyclerView.Adapter<GradesAdapter.GradeViewHolder> {

    class GradeViewHolder extends RecyclerView.ViewHolder {

        TextView mName;
        TextView mGrade;
        ImageView mOk;
        ImageView mNotOk;

        GradeViewHolder(View itemView) {
            super(itemView);

            mName = (TextView) itemView.findViewById(R.id.name);
            mGrade = (TextView) itemView.findViewById(R.id.grade);
            mOk = (ImageView) itemView.findViewById(R.id.ok);
            mNotOk = (ImageView) itemView.findViewById(R.id.not_ok);
        }
    }

    public interface OnOkNotOkClickListener {
        void onOkClick(int position);

        void onNotOkClick(int position);
    }

    private List<Grade> mItems;
    private boolean isCurrentSession;
    private OnOkNotOkClickListener mListener;

    public GradesAdapter(List<Grade> mItems, boolean isCurrentSession, OnOkNotOkClickListener mListener) {
        this.mItems = mItems;
        this.isCurrentSession = isCurrentSession;
        this.mListener = mListener;
    }

    public void setItems(List<Grade> mItems) {
        this.mItems = mItems;
        notifyDataSetChanged();
    }

    @Override
    public GradeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.list_item_grade, parent, false);
        return new GradeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GradeViewHolder holder, int position) {
        Grade grade = mItems.get(position);
        holder.mName.setText(grade.getCourse());
        holder.mGrade.setText(grade.getGrade() + "");
        if (isCurrentSession) {
            if (grade.isNewGrade()) {
                holder.mOk.setVisibility(View.VISIBLE);
                holder.mNotOk.setVisibility(View.VISIBLE);
                holder.mOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.onOkClick(holder.getAdapterPosition());
                    }
                });
                holder.mNotOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.onNotOkClick(holder.getAdapterPosition());
                    }
                });
            } else if (grade.getGrade() == 0) {
                holder.mGrade.setText("-");
                holder.mOk.setVisibility(View.GONE);
                holder.mNotOk.setVisibility(View.GONE);
            } else if (grade.isOk()) {
                holder.mOk.setVisibility(View.VISIBLE);
                holder.mOk.setImageResource(R.drawable.ic_check_grey);
                holder.mNotOk.setVisibility(View.GONE);
                holder.mOk.setClickable(false);
            } else {
                holder.mOk.setVisibility(View.GONE);
                holder.mNotOk.setVisibility(View.VISIBLE);
                holder.mNotOk.setImageResource(R.drawable.ic_close_grey);
                holder.mNotOk.setClickable(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }
}
