package intranet.piu.intranet.student.models;

import java.util.ArrayList;
import java.util.List;

public class Announcement {

    private static List<Announcement> sAnnouncements;

    static {
        sAnnouncements = new ArrayList<>();
        sAnnouncements.add(new Announcement("UTCN - partener in Proiectul European SALEEM (Structuration et accompagnement de l’entrepreneuriat étudiant au Maghreb)", "", "19.12.2017"));
        sAnnouncements.add(new Announcement("Proiectul - European Industrial Doctorate on Next Generation for Sustainable Automotive Electrical Actuation – INTERACT", "", "07.12.2017"));
        sAnnouncements.add(new Announcement("Diploma de Onoare pentru domnul Prof.dr.ing. Sorin Grozav - 20 de ani de activitate in cadrul programului CEEPUS", "", "07.12.2017"));
        sAnnouncements.add(new Announcement("Fulbright Romania - academic program for undergraduate students", "", "07.12.2017"));
        sAnnouncements.add(new Announcement("Facultatea de Litere, Centrul Universitar Nord Baia Mare, a primit Diploma de Excelenta la Gala Edumanager", "", "29.11.2017"));
        sAnnouncements.add(new Announcement("Saptamana Internationala la UTCN", "", "15.11.2017"));
        sAnnouncements.add(new Announcement("UTCN - partener in cadrul Salonului de Instrumente Pedagogice Digitale, 3 - 4 noiembrie 2017", "", "07.11.2017"));
        sAnnouncements.add(new Announcement("Seminarul Enhancing the Transfer of Research and Development Methods in Energy-related Clusters from Norway to Romania - EmPower Efficiency", "", "02.11.2017"));
    }

    public static List<Announcement> getsAnnouncements() {
        return sAnnouncements;
    }

    public static void addAnnouncement(Announcement announcement) {
        sAnnouncements.add(0, announcement);
    }

    private String title;
    private String description;
    private String date;

    public Announcement(String title, String description, String date) {
        this.title = title;
        this.description = description;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
