package intranet.piu.intranet.student.models;

import java.util.ArrayList;
import java.util.List;

public class Classs {

    private static List<Classs> sClasses;
    private static List<Classs> sTeacherClasses;

    static {
        sClasses = new ArrayList<>();
        sClasses.add(new Classs("CPD", "Assist.Prof.Eng. Cosmina IVAN"));
        sClasses.add(new Classs("DPP", "Lector dr. Dorin STANCIU"));
        sClasses.add(new Classs("PIU", "Assist.Prof.Eng. Teodor STEFANUT"));
        sClasses.add(new Classs("PT", "Assist.Prof.Dr.Eng. Emil CHIFU"));
        sClasses.add(new Classs("RC", "Prof.Dr.Eng. Vasile DADARLAT"));
        sClasses.add(new Classs("SD", "Prof.Dr.Eng. Ioan SALOMIE"));

        sTeacherClasses = new ArrayList<>();
        sTeacherClasses.add(new Classs("PIU", "Calculatoare romana, an 4"));
        sTeacherClasses.add(new Classs("UID", "Calculatoare engleza, an 4"));
    }

    public static List<Classs> getClasses() {
        return sClasses;
    }
    public static List<Classs> getTeacherClasses() {return sTeacherClasses;}

    private String name;
    private String teacher;

    public Classs(String name, String teaccher) {
        this.name = name;
        this.teacher = teaccher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}

