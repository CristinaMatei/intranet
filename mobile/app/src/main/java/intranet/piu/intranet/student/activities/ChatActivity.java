package intranet.piu.intranet.student.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.adapters.ChatAdapter;
import intranet.piu.intranet.student.models.Message;
import intranet.piu.intranet.student.models.Messenger;
import intranet.piu.intranet.student.models.Student;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    public static String MESSAGE_KEY = "message";
    public static String DATE_KEY = "date";
    public static String STUDENT_KEY = "student";

    private RecyclerView mRecyclerView;
    private EditText mEditText;
    private Button mButton;
    private ChatAdapter mAdapter;

    private String student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mRecyclerView = findViewById(R.id.recycler_view);
        mEditText = findViewById(R.id.et_message);
        mButton = findViewById(R.id.button_send);

        mButton.setOnClickListener(this);

        Intent intent = getIntent();
        student = intent.getStringExtra(STUDENT_KEY);
        setTitle(student);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new ChatAdapter(Message.getChat());
        mRecyclerView.setAdapter(mAdapter);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_send) {
            String message = mEditText.getText().toString();
            if (message.isEmpty())
                return;
            mEditText.setText("");
            Student s = Student.getStudent(student);
            if (Messenger.containsStudentName(student)) {
                Messenger.removeMessage(s);
            }
            Calendar calendar = Calendar.getInstance();
            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
            int minutesOfday = calendar.get(Calendar.MINUTE);
            Message m = new Message(message, hourOfDay + ":" + minutesOfday);
            Messenger.getMessages().add(0, new Messenger(s, m));
            Message.getChat().add(m);
            mAdapter.notifyDataSetChanged();
        }
    }

}
