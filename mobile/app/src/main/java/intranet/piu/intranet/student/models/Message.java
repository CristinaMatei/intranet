package intranet.piu.intranet.student.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristina on 1/4/2018.
 */

public class Message {

    public static List<Message> sChat;

    static {
        sChat = new ArrayList<>();
        sChat.add(new Message("Salut", "07:12"));
        sChat.add(new Message("Buna, ce faci?", "07:15", false));
        sChat.add(new Message("Bine, pe acasa, lucrez la un proiect. Tu?", "07:16"));
        sChat.add(new Message("Si eu la fel.", "07:18", false));
    }

    public static List<Message> getChat() {
        return sChat;
    }

    private String message;
    private String date;
    private boolean isSent;

    public Message(String message, String date, boolean isSent) {
        this.message = message;
        this.date = date;
        this.isSent = isSent;
    }

    public Message(String message, String date) {
        this.message = message;
        this.date = date;
        this.isSent = true;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }
}
