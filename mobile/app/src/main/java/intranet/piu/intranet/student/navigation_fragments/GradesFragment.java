package intranet.piu.intranet.student.navigation_fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.activities.GradesActivity;
import intranet.piu.intranet.student.adapters.SessionsAdapter;
import intranet.piu.intranet.student.models.Session;
import intranet.piu.intranet.student.utils.NotificationUtils;

public class GradesFragment extends Fragment implements SessionsAdapter.OnSessionClickListener {

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    public GradesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grades, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new SessionsAdapter(Session.getSessions(), this);
        mRecyclerView.setAdapter(mAdapter);
        view.findViewById(R.id.button_notif).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationUtils.sendNotification(getContext(), "New Grade Available!");
            }
        });
        return view;
    }

    @Override
    public void onSessionClick(String session) {
        Intent intent = new Intent(getActivity(), GradesActivity.class);
        intent.putExtra(GradesActivity.SESSION_KEY, session);
        startActivity(intent);
    }
}
