package intranet.piu.intranet.student.navigation_fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.activities.ClassActivity;
import intranet.piu.intranet.student.adapters.AnnouncementsAdapter;
import intranet.piu.intranet.student.adapters.ClassesAdapter;
import intranet.piu.intranet.student.models.Announcement;
import intranet.piu.intranet.student.models.Classs;

public class ClassesFragment extends Fragment implements ClassesAdapter.OnClassClickListener {

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    public ClassesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_classes, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new ClassesAdapter(Classs.getClasses(), this);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onClassClick(Classs classs) {
        Intent intent = new Intent(getActivity(), ClassActivity.class);
        intent.putExtra(ClassActivity.COURSE_KEY, classs.getName());
        startActivity(intent);
    }
}
