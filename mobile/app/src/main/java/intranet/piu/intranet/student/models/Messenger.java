package intranet.piu.intranet.student.models;

import java.util.ArrayList;
import java.util.List;

public class Messenger {

    private static List<Messenger> sMessages;

    static {
        sMessages = new ArrayList<>();
        List<Student> students = Student.getStudents();
        sMessages.add(new Messenger(students.get(0), new Message("Ok, vorbim maine.", "09:12")));
        sMessages.add(new Messenger(students.get(1), new Message("Multumesc :)", "Monday")));
        sMessages.add(new Messenger(students.get(2), new Message("Sigur, iti scriu cand aflu.", "Sunday")));
        sMessages.add(new Messenger(students.get(3), new Message("Papa", "1 Ian")));
        sMessages.add(new Messenger(students.get(4), new Message("Noapte buna!", "31 Dec")));
        sMessages.add(new Messenger(students.get(5), new Message("Salut", "20 Dec")));
        sMessages.add(new Messenger(students.get(6), new Message("Nicio problema.", "8 Dec")));
        sMessages.add(new Messenger(students.get(7), new Message("Mersi!", "7 Dec")));
    }

    public static List<Messenger> getMessages() {
        return sMessages;
    }

    public static boolean containsStudentName(String name) {
        for (Messenger m : sMessages) {
            if (m.getStudent().getName().equals(name))
                return true;
        }
        return false;
    }

    public static void removeMessage(Student s) {
        for (Messenger m : sMessages) {
            if (m.getStudent().equals(s)) {
                sMessages.remove(m);
                break;
            }
        }
    }

    private Student student;
    private Message message;

    public Messenger(Student student, Message message) {
        this.student = student;
        this.message = message;
    }

    public static List<Messenger> getsMessages() {
        return sMessages;
    }

    public static void setsMessages(List<Messenger> sMessages) {
        Messenger.sMessages = sMessages;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
