package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.models.Announcement;

/**
 * Created by Diana on 29-Dec-17.
 */

public class AnnouncementsAdapter extends RecyclerView.Adapter<AnnouncementsAdapter.AnnouncementsViewHolder> {

    class AnnouncementsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mTitle;
        TextView mDate;

        public AnnouncementsViewHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.title);
            mDate = (TextView) itemView.findViewById(R.id.date);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onAnnouncementClick();
        }
    }

    public interface OnAnnouncementClickListener {
        void onAnnouncementClick();
    }

    private List<Announcement> items;
    private OnAnnouncementClickListener mListener;

    public AnnouncementsAdapter(List<Announcement> items, OnAnnouncementClickListener listener) {
        this.items = items;
        this.mListener = listener;
    }

    public void setItems(List<Announcement> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public AnnouncementsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.list_item_announcement, parent, false);
        return new AnnouncementsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AnnouncementsViewHolder holder, int position) {
        Announcement announcement = items.get(position);
        holder.mTitle.setText(announcement.getTitle());
        holder.mDate.setText(announcement.getDate());
    }

    @Override
    public int getItemCount() {
        if (items != null) {
            return items.size();
        }
        return 0;
    }
}
