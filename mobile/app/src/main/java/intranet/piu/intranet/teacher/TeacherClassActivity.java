package intranet.piu.intranet.teacher;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.activities.MessengerActivity;
import intranet.piu.intranet.student.activities.PDFActivity;
import intranet.piu.intranet.student.adapters.CourseLabAdapter;
import intranet.piu.intranet.student.models.Message;
import intranet.piu.intranet.student.models.Messenger;
import intranet.piu.intranet.student.models.Student;
import intranet.piu.intranet.student.navigation_fragments.AnnouncementsFragment;
import intranet.piu.intranet.teacher.navigation_fragments.TeacherAnnouncementsFragment;

public class TeacherClassActivity extends AppCompatActivity implements View.OnClickListener {

    public static String COURSE_KEY = "course_name";
    public static boolean IS_PRESENCE_MARKED = false;

    private Button mButton;
    private TextView mPresentTextView;
    private ImageView mCourseImageView;
    private ImageView mLaboratoryImageView;
    private ListView mCourseListView;
    private ListView mLaboratoryListView;

    private boolean isCourseExtended = false;
    private boolean isLaboratoryExtended = false;

    private static List<String> getCourses(String classs) {
        List<String> list = new ArrayList<>();
        list.add(classs + "_Course_1");
        list.add(classs + "_Course_2");
        list.add(classs + "_Course_3");
        list.add(classs + "_Course_4");
        list.add(classs + "_Course_5");
        list.add(classs + "_Course_6");
        list.add(classs + "_Course_7");
        list.add(classs + "_Course_8");
        list.add(classs + "_Course_9");
        list.add(classs + "_Course_10");
        list.add(classs + "_Course_11");
        list.add(classs + "_Course_12");
        return list;
    }

    private static List<String> getLaboratories(String classs) {
        List<String> list = new ArrayList<>();
        list.add(classs + "_Laboratory_1");
        list.add(classs + "_Laboratory_2");
        list.add(classs + "_Laboratory_3");
        list.add(classs + "_Laboratory_4");
        list.add(classs + "_Laboratory_5");
        list.add(classs + "_Laboratory_6");
        list.add(classs + "_Laboratory_7");
        list.add(classs + "_Laboratory_8");
        list.add(classs + "_Laboratory_9");
        list.add(classs + "_Laboratory_10");
        list.add(classs + "_Laboratory_11");
        list.add(classs + "_Laboratory_12");
        return list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_teacher);

        String course = getIntent().getStringExtra(COURSE_KEY);

        mButton = findViewById(R.id.button_announcement);
        mPresentTextView = findViewById(R.id.tv_present);
        mCourseImageView = findViewById(R.id.course_icon);
        mLaboratoryImageView = findViewById(R.id.laboratory_icon);
        mCourseListView = findViewById(R.id.course_list);
        mLaboratoryListView = findViewById(R.id.laboratory_list);

        mCourseListView.setAdapter(new CourseLabAdapter(getCourses(course), this));
        mCourseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(TeacherClassActivity.this,PDFActivity.class));
            }
        });
        mLaboratoryListView.setAdapter(new CourseLabAdapter(getLaboratories(course), this));
        mLaboratoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(TeacherClassActivity.this,PDFActivity.class));
            }
        });

        findViewById(R.id.course_relative_layout).setOnClickListener(this);
        findViewById(R.id.laboratory_relative_layout).setOnClickListener(this);
        findViewById(R.id.anunturi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TeacherAnnouncementsFragment fragment = new TeacherAnnouncementsFragment();
                setTitle(R.string.title_announcements);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.class_teacher_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();

                mButton.setVisibility(View.GONE);
            }
        });

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(course);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(TeacherClassActivity.this);
                dialog.setContentView(R.layout.layout_announcement);
                dialog.setTitle("Anunt nou");

                final EditText editText = (EditText) dialog.findViewById(R.id.announcement_message);

                Button sendButton = (Button) dialog.findViewById(R.id.button_send_announcement);
                sendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String message = editText.getText().toString();
                        if (message.isEmpty()) {
                            new AlertDialog.Builder(TeacherClassActivity.this)
                                    .setMessage("Error: empty message!")
                                    .setPositiveButton("OK", null)
                                    .show();
                        } else {
                            new AlertDialog.Builder(TeacherClassActivity.this)
                                    .setMessage("Are you sure you want to post this announcement?")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Calendar calendar = Calendar.getInstance();
                                            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
                                            int minutesOfday = calendar.get(Calendar.MINUTE);
                                            Message m = new Message(message, hourOfDay + ":" + minutesOfday);

                                            //mAdapter.setItems(Messenger.getMessages());
                                        }
                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                        }
                        dialog.dismiss();
                    }
                });
                Button cancelButton = (Button) dialog.findViewById(R.id.button_cancel_announcement);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

        @Override
        public void onBackPressed () {
            mButton.setVisibility(View.VISIBLE);
            super.onBackPressed();
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            int id = item.getItemId();
            if (id == android.R.id.home) {
                onBackPressed();
            }
            return super.onOptionsItemSelected(item);

        }

        @Override
        public void onClick (View view){
            switch (view.getId()) {
                case R.id.button_announcement: {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    final EditText editText = new EditText(this);
                    alert.setTitle("Enter the key");
                    alert.setView(editText);

                    alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            final String key = editText.getText().toString();

                            if (key.isEmpty()) {
                                new AlertDialog.Builder(TeacherClassActivity.this)
                                        .setMessage("Cannot send an empty key!")
                                        .setPositiveButton("OK", null)
                                        .show();
                            } else {
                                new AlertDialog.Builder(TeacherClassActivity.this)
                                        .setMessage("Are you sure you want to send this key?")
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (key.equals("1234")) {
                                                    mButton.setVisibility(View.GONE);
                                                    mPresentTextView.setVisibility(View.VISIBLE);
                                                    IS_PRESENCE_MARKED = true;
                                                } else {
                                                    new AlertDialog.Builder(TeacherClassActivity.this)
                                                            .setMessage("Incorrect key!")
                                                            .setPositiveButton("OK", null)
                                                            .show();
                                                }
                                            }
                                        })
                                        .setNegativeButton("No", null)
                                        .show();
                            }
                        }
                    });
                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
                    alert.show();
                    break;
                }
                case R.id.course_relative_layout: {
                    if (isCourseExtended) {
                        mCourseImageView.setImageResource(R.drawable.ic_arrow_right);
                        mCourseListView.setVisibility(View.GONE);
                    } else {
                        mCourseImageView.setImageResource(R.drawable.ic_arrow_down);
                        mCourseListView.setVisibility(View.VISIBLE);
                    }
                    isCourseExtended = !isCourseExtended;
                    break;
                }
                case R.id.laboratory_relative_layout: {
                    if (isLaboratoryExtended) {
                        mLaboratoryImageView.setImageResource(R.drawable.ic_arrow_right);
                        mLaboratoryListView.setVisibility(View.GONE);
                    } else {
                        mLaboratoryImageView.setImageResource(R.drawable.ic_arrow_down);
                        mLaboratoryListView.setVisibility(View.VISIBLE);
                    }
                    isLaboratoryExtended = !isLaboratoryExtended;
                    break;
                }
            }
        }
    }
