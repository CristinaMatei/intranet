package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.models.Message;
import intranet.piu.intranet.student.models.Messenger;

public class MessengerAdapter extends RecyclerView.Adapter<MessengerAdapter.MessengerViewHolder> {

    class MessengerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView mImageView;
        TextView mName;
        TextView mMessage;
        TextView mDate;

        public MessengerViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.iv_user_photo);
            mName = itemView.findViewById(R.id.tv_name);
            mMessage = itemView.findViewById(R.id.message);
            mDate = itemView.findViewById(R.id.date_time);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onMessageClicked(mItems.get(position), position);
        }
    }

    public interface OnMessageClick {
        void onMessageClicked(Messenger messenger, int position);
    }

    private List<Messenger> mItems;
    private OnMessageClick mListener;

    public MessengerAdapter(List<Messenger> mItems, OnMessageClick mListener) {
        this.mItems = mItems;
        this.mListener = mListener;
    }

    public void addItem(Messenger messenger) {
        mItems.add(0, messenger);
        notifyDataSetChanged();
    }

    public void changeItem(Message message, int position) {
        mItems.get(position).setMessage(message);
        notifyDataSetChanged();
    }

    public void setItems(List<Messenger> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public MessengerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.list_item_messenger, parent, false);
        return new MessengerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessengerViewHolder holder, int position) {
        Messenger messenger = mItems.get(position);
        holder.mDate.setText(messenger.getMessage().getDate());
        holder.mMessage.setText(messenger.getMessage().getMessage());
        holder.mName.setText(messenger.getStudent().getName());
        holder.mImageView.setImageResource(messenger.getStudent().getPhoto());
    }

    @Override
    public int getItemCount() {
        if (mItems != null)
            return mItems.size();
        return 0;
    }
}
