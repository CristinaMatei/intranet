package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.models.Message;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

    class ChatViewHolder extends RecyclerView.ViewHolder {

        TextView message;
        TextView date;

        public ChatViewHolder(View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.message);
            date = itemView.findViewById(R.id.time);
        }
    }

    private List<Message> mItems;

    public ChatAdapter(List<Message> mItems) {
        this.mItems = mItems;
    }

    public void addItem(Message message) {
        mItems.add(message);
        notifyDataSetChanged();
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view;
        if (viewType == 0)
            view = inflater.inflate(R.layout.list_item_chat1, parent, false);
        else
            view = inflater.inflate(R.layout.list_item_chat2, parent, false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        Message message = mItems.get(position);
        holder.message.setText(message.getMessage());
        holder.date.setText(message.getDate());
    }

    @Override
    public int getItemCount() {
        if (mItems != null)
            return mItems.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.get(position).isSent())
            return 1;
        return 0;
    }
}
