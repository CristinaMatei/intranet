package intranet.piu.intranet.student.models;


import java.util.ArrayList;
import java.util.List;

import intranet.piu.intranet.R;

public class Student {

    private static List<Student> sStudents;

    static {
        sStudents = new ArrayList<>();
        sStudents.add(new Student("Maria Popa", R.drawable.user_1));
        sStudents.add(new Student("Cristina Stanciu", R.drawable.user_2));
        sStudents.add(new Student("Bogdan Coman", R.drawable.user_3));
        sStudents.add(new Student("Rares Voicu", R.drawable.user_4));
        sStudents.add(new Student("Andreea Raita", R.drawable.user_5));
        sStudents.add(new Student("Ciprian Paun", R.drawable.user_6));
        sStudents.add(new Student("Alexandru Pop", R.drawable.user_7));
        sStudents.add(new Student("Diana Lazar", R.drawable.user_8));
    }

    public static List<Student> getStudents() {
        return sStudents;
    }

    public static List<String> getStudentsName() {
        List<String> names = new ArrayList<>();
        for (Student s : sStudents) {
            names.add(s.getName());
        }
        return names;
    }

    public static boolean containsStudentName(String name) {
        for (Student s : sStudents) {
            if (s.getName().equals(name))
                return true;
        }
        return false;
    }

    public static Student getStudent(String name) {
        for (Student s : sStudents) {
            if (s.getName().equals(name))
                return s;
        }
        return null;
    }

    private String name;
    private int photo;

    public Student(String name, int photo) {
        this.name = name;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
