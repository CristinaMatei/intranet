package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.models.Student;

public class GroupAdapter extends BaseAdapter {

    private List<Student> mItems;
    private Context mContext;

    public GroupAdapter(List<Student> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (mItems != null) {
            return mItems.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_group, viewGroup, false);
        }
        Student student = mItems.get(i);
        ((TextView) view.findViewById(R.id.tv_name)).setText(student.getName());
        ((ImageView) view.findViewById(R.id.iv_user_photo)).setImageResource(student.getPhoto());

        return view;
    }
}
