package intranet.piu.intranet.student;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import intranet.piu.intranet.LoginActivity;
import intranet.piu.intranet.R;
import intranet.piu.intranet.student.activities.GroupActivity;
import intranet.piu.intranet.student.activities.MessengerActivity;
import intranet.piu.intranet.student.adapters.DrawerListAdapter;
import intranet.piu.intranet.student.navigation_fragments.AnnouncementsFragment;
import intranet.piu.intranet.student.navigation_fragments.ClassesFragment;
import intranet.piu.intranet.student.navigation_fragments.GradesFragment;
import intranet.piu.intranet.student.navigation_fragments.ScheduleFragment;

public class StudentActivity extends AppCompatActivity {

    private static final int POSITION_ANNOUNCEMENTS = 0;
    private static final int POSITION_CLASSES = 1;
    private static final int POSITION_GRADES = 2;
    private static final int POSITION_SCHEDULE = 3;
    private static final int POSITION_MATES = 4;
    private static final int POSITION_LOGOUT = 5;

    private static final String NAVIGATION_ITEM_NAME = "nav_name";

    private Fragment mFragment;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ActionBar mActionBar;
    private Toolbar mToolbar;
    private ListView mDrawerList;
    BottomNavigationView mNavigation;

    boolean doubleBackToExitPressedOnce = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_announcements:
                    mFragment = new AnnouncementsFragment();
                    setTitle(R.string.title_announcements);
                    break;
                case R.id.navigation_classes:
                    mFragment = new ClassesFragment();
                    setTitle(R.string.title_classes);
                    break;
                case R.id.navigation_grades:
                    mFragment = new GradesFragment();
                    setTitle(R.string.title_grades);
                    break;
                case R.id.navigation_schedule:
                    mFragment = new ScheduleFragment();
                    setTitle(R.string.title_schedule);
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, mFragment);
            transaction.commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        mNavigation = (BottomNavigationView) findViewById(R.id.navigation);
        mNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mFragment = new AnnouncementsFragment();
        setTitle(R.string.title_announcements);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, mFragment);
        transaction.commit();

        setupActionBar();
        setupDrawer();
    }

    @Override
    public void onBackPressed() {
        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit!", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.student_action_menu, menu);
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        final SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        String dataArr[] = {"Announcements", "Grades", "Classes", "Schedule", "Messenger", "Group Mates"};
        ArrayAdapter<String> newsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, dataArr);
        searchAutoComplete.setAdapter(newsAdapter);
        searchAutoComplete.setTextColor(Color.WHITE);
        searchAutoComplete.setDropDownBackgroundResource(android.R.color.white);
        searchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemIndex, long id) {
                String queryString = (String) adapterView.getItemAtPosition(itemIndex);
                searchAutoComplete.setText(queryString);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconified(true);
                searchView.onActionViewCollapsed();
                search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search: {
                return true;
            }
            case R.id.action_messenger: {
                startActivity(new Intent(this, MessengerActivity.class));
                return true;
            }
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeButtonEnabled(true);
        }
    }

    private void setupDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_view);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                mDrawerLayout.bringToFront();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        String[] items = getResources().getStringArray(R.array.drawer_items);
        DrawerListAdapter adapter = new DrawerListAdapter(items, this);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case POSITION_ANNOUNCEMENTS: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mNavigation.setSelectedItemId(R.id.navigation_announcements);
                        break;
                    }
                    case POSITION_CLASSES: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mNavigation.setSelectedItemId(R.id.navigation_classes);
                        break;
                    }
                    case POSITION_GRADES: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mNavigation.setSelectedItemId(R.id.navigation_grades);
                        break;
                    }
                    case POSITION_SCHEDULE: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mNavigation.setSelectedItemId(R.id.navigation_schedule);
                        break;
                    }
                    case POSITION_MATES: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        startActivity(new Intent(StudentActivity.this, GroupActivity.class));
                        break;
                    }
                    case POSITION_LOGOUT: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        logOut();
                        break;
                    }
                }
            }
        });
    }

    private void logOut() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.key_username), "");
        editor.apply();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void search(String query) {
        if (getString(R.string.title_announcements).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_announcements);
        } else if (getString(R.string.title_grades).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_grades);
        } else if (getString(R.string.title_classes).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_classes);
        } else if (getString(R.string.title_schedule).equals(query)) {
            mNavigation.setSelectedItemId(R.id.navigation_schedule);
        } else if ("Group Mates".equals(query)) {
            startActivity(new Intent(this, GroupActivity.class));
        } else if ("Messenger".equals(query)) {
            startActivity(new Intent(this, MessengerActivity.class));
        }
    }
}
