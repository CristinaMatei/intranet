package intranet.piu.intranet.student.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.EditText;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.adapters.GradesAdapter;
import intranet.piu.intranet.student.models.Grade;
import intranet.piu.intranet.student.models.Session;

public class GradesActivity extends AppCompatActivity implements GradesAdapter.OnOkNotOkClickListener {

    public static final String SESSION_KEY = "session";

    RecyclerView mRecyclerView;
    GradesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grades);

        mRecyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        String session = getIntent().getStringExtra(SESSION_KEY);
        if (Session.FIRST_YEAR.equals(session)) {
            mAdapter = new GradesAdapter(Grade.getFisrtYearGrades(), false, this);
        } else if (Session.SECOND_YEAR.equals(session)) {
            mAdapter = new GradesAdapter(Grade.getSecondYearGrades(), false, this);
        } else if (Session.THIRD_YEAR.equals(session)) {
            mAdapter = new GradesAdapter(Grade.getThirdYearGrades(), false, this);
        } else {
            mAdapter = new GradesAdapter(Grade.getCurrentGrades(), true, this);
            session = "Current session";
        }
        mRecyclerView.setAdapter(mAdapter);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(session);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onOkClick(final int position) {
        new AlertDialog.Builder(GradesActivity.this)
                .setMessage("Are you sure you agree with this grade?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Grade grade = Grade.getCurrentGrades().get(position);
                        grade.setNewGrade(false);
                        grade.setOk(true);
                        mAdapter = new GradesAdapter(Grade.getCurrentGrades(), true, GradesActivity.this);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                })
                .show();
    }

    @Override
    public void onNotOkClick(final int position) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText editText = new EditText(this);
        alert.setTitle("Write a comment");

        alert.setView(editText);

        alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String message = editText.getText().toString();

                if (message.isEmpty()) {
                    new AlertDialog.Builder(GradesActivity.this)
                            .setMessage("Cannot send an empty comment!")
                            .setPositiveButton("OK", null)
                            .show();
                } else {
                    new AlertDialog.Builder(GradesActivity.this)
                            .setMessage("Are you sure you want to send this comment?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Grade grade = Grade.getCurrentGrades().get(position);
                                    grade.setNewGrade(false);
                                    grade.setOk(false);
                                    mAdapter = new GradesAdapter(Grade.getCurrentGrades(), true, GradesActivity.this);
                                    mRecyclerView.setAdapter(mAdapter);

                                    // Implement sending the message to the teacher
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }
}
