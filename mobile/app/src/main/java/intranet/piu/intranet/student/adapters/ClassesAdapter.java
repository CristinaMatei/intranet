package intranet.piu.intranet.student.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.models.Classs;

public class ClassesAdapter extends RecyclerView.Adapter<ClassesAdapter.ClassViewHolder> {

    class ClassViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {

        TextView mName;
        TextView mTeacher;

        public ClassViewHolder(View itemView) {
            super(itemView);

            mName = (TextView) itemView.findViewById(R.id.name);
            mTeacher = (TextView) itemView.findViewById(R.id.teacher);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onClassClick(items.get(position));
        }
    }

    public interface OnClassClickListener {
        void onClassClick(Classs classs);
    }

    private List<Classs> items;
    private OnClassClickListener mListener;

    public ClassesAdapter(List<Classs> items, OnClassClickListener mListener) {
        this.items = items;
        this.mListener = mListener;
    }

    public void setItems(List<Classs> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public ClassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.list_item_class, parent, false);
        return new ClassViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClassViewHolder holder, int position) {
        Classs classs = items.get(position);
        holder.mName.setText(classs.getName());
        holder.mTeacher.setText(classs.getTeacher());
    }

    @Override
    public int getItemCount() {
        if (items != null) {
            return items.size();
        }
        return 0;
    }
}
