package intranet.piu.intranet.student.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import intranet.piu.intranet.R;
import intranet.piu.intranet.student.adapters.GroupAdapter;
import intranet.piu.intranet.student.models.Student;

public class GroupActivity extends AppCompatActivity {

    ListView mListView;
    GroupAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        setTitle("Group Mates");

        mListView = findViewById(R.id.list_view);
        mAdapter = new GroupAdapter(Student.getStudents(), this);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(GroupActivity.this, ChatActivity.class);
                intent.putExtra(ChatActivity.STUDENT_KEY, Student.getStudents().get(i).getName());
                startActivity(intent);
            }
        });

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
