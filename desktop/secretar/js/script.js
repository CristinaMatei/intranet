function logout() {
  window.location.href = "../login.html"; 
}

function homepage() {
  window.location.href = "./start.html"; 
}

function validareNume(value){
	if(/^[A-Za-z]+$/.test(value)){  
		return true;
	}
	
	return false;
}

function validareCNP(value){
	if(/^[0-9]+$/.test(value) && value.length === 13) {
		return true;
	}
	
	return false;
}

function validareSerieBuletin(value){
	if(/^[A-Za-z]+$/.test(value) && value.length === 2){  
		return true;
	}
	
	return false;
}

function validareNumarBuletin(value){
	if(/^[0-9]+$/.test(value) && value.length === 6) {
		return true;
	}
	
	return false;
}

function validareNumarTelefon(value){
	if(/^[0-9]+$/.test(value) && value.length === 10) {
		return true;
	}
	
	return false;
}

function validareEmail(value){
	if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
		return true;
	}
	
	return false;
}

$(document).ready( function () {
	var animationOn = false;
	var formtHeight;
	var inconWidth;
	var elemHeight;
	var displayElem;
	var elemPadding;
	
	
	$('#adaugare').click(function(){
		var errorMessage = "";
		
		var nume = document.getElementById('nume').value;
		var prenume = document.getElementById('prenume').value;
	    var cnp = document.getElementById('cnp').value;
 		var serie = document.getElementById('serie').value;
 		var numar = document.getElementById('numar').value;
 		var telefon = document.getElementById('telefon').value;
		var email = document.getElementById('email').value;
		
		if(!validareNume(nume)){
			errorMessage+="Numele trebuie sa contina doar litere. \n";
		} 
		
		if(!validareNume(prenume)){
			errorMessage+="Prenumele trebuie sa contina doar litere. \n";
		}

		if(!validareCNP(cnp)){
			errorMessage+="CNP-ul trebuie sa contina 13 cifre. \n";
		}

		if(!validareSerieBuletin(serie)){
			errorMessage+="Seria de buletin trebuie sa contina 2 litere. \n";
		}

		if(!validareNumarBuletin(numar)){
			errorMessage+="Numarul de buletin trebuie sa contina 6 cifre. \n";
		}

		if(!validareNumarTelefon(telefon)){
			errorMessage+="Numarul de telefon trebuie sa contina 10 cifre. \n";
		}
		
		if(!validareEmail(email)){
			errorMessage+="Emailul introdus este incorect. \n";
		}

		if(errorMessage === ""){
			alert("Adaugare cadrului didactic s-a realizat cu succes!");
		} else {
			errorMessage += "\nREINTRODUCETI DATELE!"
			alert(errorMessage);
			return false;
		}
});

	$('#inrolare').on("click",function(){
			alert("Inrolarea studentului s-a realizat cu succes!");
});
	
	$(".fa-search").on("click", function() {
		if(!animationOn) {
			animationOn = true;		
		} else {
			animationOn = false;
		}
		
		if(animationOn) {
			$('.input-search').animate({
				width:'180px'				
			}, 500, function() {		
			$('.input-search').css("display", "block");
			})
		} else {
			$('.input-search').animate({
				width:'0px'				
			}, 500, function() {		
			$('.input-search').css("display", "none");
			})
		}
		
	});	 
		
	$('.input-search').on('keyup', function (e) {
		if (e.keyCode == 13) {
			var key = $('.input-search').val();
				if(key.match("adaugare") || key.match("profesor")) {
					window.location.href = "./adaugare_profesor.html"; 
					 $('.input-search').val("");
				} else if(key.match("inregistrare") || key.match("student")) {
					window.location.href = "./inregistrare_student.html"; 
					 $('.input-search').val("");
				} else {
					window.location.href = "./not_found.html"; 
				}
		}
	});
		
});