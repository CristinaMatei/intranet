var merge = false;

function validareNota(value){
	if(value >= 1 && value <= 10) {
		return true;
	}
	
	return false;
}

function logout() {
  window.location.href = "../login.html"; 
}

function homepage() {
  window.location.href = "./start.html"; 
}

function stergeCurs1() {
    var r = confirm("Stergeti \"Cursul 1. Inițiere în rețele de calculatoare.\"?");
    if (r == true) {
        var card = document.getElementById("card_curs1");
        card.style.visibility = 'hidden';
    } else {
        
    }
}
function stergeCurs2() {
    var r = confirm("Stergeti \"Cursul 2. Istoria rețelelor.\"?");
    if (r == true) {
        var card = document.getElementById("card_curs2");
        card.style.visibility = 'hidden';
    } else {
        
    }
}
function stergeCurs3() {
    var r = confirm("Stergeti \"Cursul 3. Protocoale de comunicare I.\"?");
    if (r == true) {
        var card = document.getElementById("card_curs3");
        card.style.visibility = 'hidden';
    } else {
        
    }
}

function adaugaCurs() {
	var addButton =  document.getElementById("addButton");
	var browser = document.getElementById("browseForCurs");

	addButton.style.order = 3;
	browser.style.order = 2;
	addButton.style.visibility = "hidden";
	browser.style.visibility = "visible";
}


function onBrowsedCurs(myFile) {
	var file = myFile.files[0];  
   	var filename = file.name;

   	var addButton =  document.getElementById("addButton");
	var browser = document.getElementById("browseForCurs");
	var card = document.getElementById("card_curs6");

	//var numeCurs = document.getElementById("title_card_curs6");
	//numeCurs.innerHTML(filename);

	addButton.style.order = 3;
	browser.style.order = 4;
	addButton.style.visibility = "visible";
	browser.style.visibility = "hidden";

	card.style.order = 2;
	card.style.visibility = "visible";
}

function adaugaLaborator() {
	var addButton =  document.getElementById("addButtonLab");
	var browser = document.getElementById("browseForLab");

	addButton.style.order = 3;
	browser.style.order = 2;
	addButton.style.visibility = "hidden";
	browser.style.visibility = "visible";
}

function onBrowsedLab(myFile) {
	var file = myFile.files[0];  
   	var filename = file.name;

   	var addButton =  document.getElementById("addButtonLab");
	var browser = document.getElementById("browseForLab");
	var card = document.getElementById("card_lab6");

	//var numeCurs = document.getElementById("title_card_curs6");
	//numeCurs.innerHTML(filename);

	addButton.style.order = 3;
	browser.style.order = 4;
	addButton.style.visibility = "visible";
	browser.style.visibility = "hidden";

	card.style.order = 2;
	card.style.visibility = "visible";
}

function startSession() {
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];
	span.onclick = function() {
    	modal.style.display = "none";
	}
	var cancel_btn = document.getElementById("cancel-btn");
	cancel_btn.onclick = function() {
    	modal.style.display = "none";
	}
	modal.style.display = "block";
}
function onStartSession(b) {
	document.getElementById("start-btn").innerHTML = "Start";

	var p = document.getElementById("start_session");
	p.style.visibility = "visible";

	if (merge) {
		var modal = document.getElementById('myModal');
		modal.style.display = "none";

		document.getElementById("info_prezenta").style.visibility = "visible";
		document.getElementById("a_prezenta1").href = "https://docs.google.com/spreadsheets/d/1NUvbm93Ngxo9jpxI55cDqCtxzd2hpPHs_MmRDExRsdI/edit#gid=0";
	} else {
		document.getElementById("start-btn").innerHTML = "Finalizare";
	}

	merge = true;
}

$(document).ready( function () {
	
	$('#add_note').on("click",function(){
		var errorMessage = "";
		var nota1 = document.getElementById('nota1').value;
		var nota2 = document.getElementById('nota2').value;
		var nota3 = document.getElementById('nota3').value;
		var nota4 = document.getElementById('nota4').value;
		var nota5 = document.getElementById('nota5').value;
	
		
		
		if(!validareNota(nota1) || !validareNota(nota2) || !validareNota(nota3) || !validareNota(nota4) || !validareNota(nota5)){
			errorMessage+="Nota trebuie sa fie cuprinsa intre 1 si 10. \n";
		} 
		
		
		if(errorMessage === ""){
			alert("Adaugare notelor s-a realizat cu succes!");
			document.getElementById("nota1").disabled = true;
			document.getElementById("nota2").disabled = true;
			document.getElementById("nota3").disabled = true;
			document.getElementById("nota4").disabled = true;
			document.getElementById("nota5").disabled = true;
			document.getElementById("reply_button").disabled = false;
			document.getElementById("comentariu").innerHTML = "As dori sa imi vad lucrarea!";
			return true;
		} else {
			errorMessage += "\nREINTRODUCETI NOTELE!"
			alert(errorMessage);
		}
});

	$('#reply_button').on("click",function(){
		var message = prompt("Please enter your message:", "");

		if (message != null && message != "") {
		    alert("Mesajul s-a trimis cu succes!");
			document.getElementById('reply_button').disabled = 'true';
			document.getElementById('reply_button').style.backgroundColor = "green"
		} else {
			alert("Retrimiteti mesajul!");
		}
});

});
