var nrCard;
var lunaExp;
var anExp;
var codCw2;
var numeCard;
var formHeight;
var formular;
var initialValue;
var anSuma;
var caminLunaCurentaSuma=155;
var caminLunaRestantaSuma;
var marireSuma=100;
var restantaSuma=125;
var sumaTotalaPlataAn ;
var fonduri;
var incercari;

function logout() {
  window.location.href = "../login.html"; 
}

function homepage() {
  window.location.href = "./start.html"; 
}


function testIfNumber(value){
	if(/^[0-9]+$/.test(value)) {
		return true;
	}
	
	return false;
}

function cardNameValidation(value){
	if(/^[A-Za-z ]+$/.test(value)){  
		return true;
	}
	
	return false;
}

function cw2LenghtValidation(value){
	
	var isNumber = testIfNumber(value);
	
	if(isNumber === true && value.length === 3 ) {
		return true;
	 }
	 return false;
 }
 
function cardNoLenghtValidation(value){
	var isNumber = testIfNumber(value);
	
	if(isNumber === true && value.length === 16 ) {
		return true;
	 }
	 return false;
}

function sumValidation(value){
	var isNumber = testIfNumber(value);
	
	if(isNumber === true) {
		return true;
	} 
	else {
		return false;
	}
}

function validateFormData(){
var isValid = true;
	if(!cardNameValidation(numeCard)) {
		if($('.nume-card').css('display')==='none') {
			$(".nume-card").css("display", "block");
			formHeight = formular.height() + 30 ;
			formular.height(formHeight);
		}
		isValid = false;
	}
	 else {		 
		if($('.nume-card').css('display')==='block') {
			formHeight = formular.height() - 30 ;
			formular.height(formHeight);
			$(".nume-card").css("display", "none");
		}
	 }
	
	if(!cw2LenghtValidation(codCw2)) {
		if($('.cod').css('display')==='none') {
			$(".cod").css("display", "block");
			formHeight = formular.height() + 30 ;
			formular.height(formHeight);
		}
		isValid = false;
	}
	 else {	
		if($('.cod').css('display')==='block') {
			$(".cod").css("display", "none");
			formHeight = formular.height() - 30 ;
			formular.height(formHeight);
		}
	 }
	
	if(!cardNoLenghtValidation(nrCard)) {
		if($('.nr-card').css('display')==='none') {
			$(".nr-card").css("display", "block");
			formHeight = formular.height() + 30 ;
			formular.height(formHeight);
		}
		
		isValid = false;
	}	
	 else {
		 if($('.nr-card').css('display')==='block') {
			$(".nr-card").css("display", "none");
			formHeight = formular.height() - 30 ;
			formular.height(formHeight);
		}
	 }
	 
	 return isValid;
}

function displayTooltip(){
   $("#nume").tooltip({ content: '<img src="./img/name_front-face-card.png" />' });
   
   $( "#nume" ).tooltip( "option", "position", { my: "left+15 center", at: "right center" } ); 

   $("#cod-cw2").tooltip({ content: '<img src="./img/rsz_1rsz_back-face-card.png" />' });
   
   $( "#cod-cw2" ).tooltip( "option", "position", { my: "left+15 center", at: "right center" } );

    $("#data-expirare-card").tooltip({ content: '<img src="./img/date_front-face-card.png" />' });
   
    $( "#data-expirare-card" ).tooltip( "option", "position", { my: "left+15 center", at: "right center" } );

    $("#nr-card").tooltip({ content: '<img src="./img/card-no_front-face-card.png" />' });
   
    $( "#nr-card" ).tooltip( "option", "position", { my: "left+15 center", at: "right center" } );
}

function displayPopUp(){ 
		// CLOSE POP-UP
		$('[data-popup-close]').on('click', function(e)  {
			var targeted_popup_class = jQuery(this).attr('data-popup-close');
			$('[data-popup="popup-message"]').fadeOut(350);
			e.preventDefault();
		});
	
}

function displayLoadingIcon(){
		// LOADING IMAGE
		 $('[data-popup-open]').on('click', function(e)  {	 
			var targeted_popup_class = jQuery(this).attr('data-popup-open');
			$('[data-popup="popup-loading"]').fadeIn(350).fadeOut(650, 	
		// OPEN POP-UP
			function(){
				$('[data-popup="popup-message"]').fadeIn(350)	
			}	
			);
			e.preventDefault();
		});

}


function setPopUpMessage(title, message){
	$('.form-h2-msg').text(title);
	$('.form-p-msg').text(message);
}


function validateSumForYearPayment(){
	var isSumValid = true;
	if(Number(fonduri)<=0) {
		setPopUpMessage("Fonduri insificiente", "Verifica daca mai ai bani in cont.");
		isSumValid = false;
	} else 
	if((parseInt(anSuma) > parseInt(fonduri)) &&(Number(anSuma) <= Number(sumaTotalaPlataAn))) {
		setPopUpMessage("Fonduri insificiente", "Suma introdusa este prea mare, raportata la suma curenta din cont.");
		isSumValid = false;
		
	} else if(Number(anSuma) > Number(sumaTotalaPlataAn)) {	
		if($('.suma-an-span').css('display')==='none') {
			formHeight = formular.height() + 30 ;
			formular.height(formHeight);
		}
		
		$(".suma-an-span").text('Valoarea introdusa este prea mare');
		$(".suma-an-span").css('display','block');
		isSumValid = false;
	
	} else if(Number(anSuma) == 0) {
		
		if($('.suma-an-span').css('display')==='none') {
			formHeight = formular.height() + 30 ;
			formular.height(formHeight);
		}
		
		$(".suma-an-span").text('Valoarea introdusa trebuie sa fie mai mare decat 0');
		$(".suma-an-span").css('display','block');
		isSumValid = false;
		
	} else {
		
		if($('.suma-an-span').css('display')==='block') {
			formHeight = formular.height() - 30 ;
			formular.height(formHeight);
		}
		
		$(".suma-an-span").css('display','none');
	
	}
	
	return isSumValid;
}

function validateSumForCaminOrRestanteMariri(){
	if(Number(fonduri)<=0) {
		setPopUpMessage("Fonduri insificiente", "Verifica daca mai ai bani in cont.");
		return false;
	} else 
	if(parseInt(anSuma) > parseInt(fonduri)) {
		setPopUpMessage("Fonduri insificiente", "Suma introdusa este prea mare, raportata la suma curenta din cont.");
		return false;
	}
	return true;
}

$(document).ready( function () {
	var animationOn = false;
	$(".fa-search").on("click", function() {
		if(!animationOn) {
			animationOn = true;		
		} else {
			animationOn = false;
		}
		
		if(animationOn) {
			$('.input-search').animate({
				width:'180px'				
			}, 500, function() {		
			$('.input-search').css("display", "block");
			})
		} else {
			$('.input-search').animate({
				width:'0px'				
			}, 500, function() {		
			$('.input-search').css("display", "none");
			})
		}
		
	});	 
		
	$('.input-search').on('keyup', function (e) {
		if (e.keyCode == 13) {
			var key = $('.input-search').val();
				if(key.match("boboc") || key.match("saptamana")) {
					window.location.href = "./boboc.html"; 
					 $('.input-search').val("");
				} else {
					window.location.href = "./not_found.html"; 
				}
		}
	});
	
	displayTooltip();
	displayPopUp();
	displayLoadingIcon();
	
	sumaTotalaPlataAn = localStorage.getItem("sumaTotalaPlataAn");
	fonduri = localStorage.getItem("fonduri");
	incercari = localStorage.getItem("incercari_marire");
	
	$(".disciplina-select option[value='NA']").hide();
	$( ".suma-ramasa-platit" ).text(localStorage.getItem("sumaTotalaPlataAn"));	
	($('.default-selection')).hide();
	$(".submit").on("click", function() {
		 nrCard =  $( ".nr-card-text" ).val().replace(/\s+/g, '');
		 lunaExp= $( ".luna-combo" ).val();
		 anExp= $( ".an-combo" ).val();
		 codCw2= $( ".cod-cw2-text" ).val();
		 numeCard= $( ".nume-card-text" ).val();
		 formular = $( ".formular" ).find( 'form' );
		 anSuma = $( ".an-sum" ).val();
		 initialValue =  formular.height();
		
		 var isValid = validateFormData();
		 
		 // Pentru taxa anuala
		 if(window.location.href.includes('an_crt.html')) {
			 if(isValid) {
				isSumValid = validateSumForYearPayment();
				if(isSumValid) {

					setPopUpMessage("Finalizare tranzactie", "Succes.");
					
					$( ".nr-card-text" ).val("");
					$( ".cod-cw2-text" ).val("");
					$( ".nume-card-text" ).val("");
					$( ".an-sum" ).val("");	
					$(".luna-combo").val($(".luna-combo option:first").val());
					$(".an-combo").val($(".an-combo option:first").val());
					
					sumaTotalaPlataAn = Number(sumaTotalaPlataAn) - Number(anSuma);
					fonduri = parseInt(fonduri) - parseInt(anSuma);
					
					localStorage.setItem("sumaTotalaPlataAn", sumaTotalaPlataAn);
					localStorage.setItem("fonduri", fonduri);
					
					console.log(sumaTotalaPlataAn);
					console.log(fonduri);
					
					$( ".suma-ramasa-platit" ).text(sumaTotalaPlataAn);	
					
					
				} 
			}else {
					setPopUpMessage("Esuat", "Verifica datele introduse.");
			}
		 }
	
		 // Pentru taxa caminului
		 if(window.location.href.includes('camin.html')) {
			 if(isValid) {
				var isCaminSumValid = validateSumForCaminOrRestanteMariri();
				var val = ($('.luna-plata-combo')).val();
				if($('.luna-plata-combo').find(":selected").text()==="N/A" || $('.luna-plata-combo').find(":selected").text()==="" ) {
					setPopUpMessage("Informatii", "Esti cu platile la zi.");
				}
				else {
				if(isCaminSumValid) {
					
					$(".luna-plata-combo option[value='"+ ($('.luna-plata-combo')).val() + "']").remove();
					
					if(val==='L1')
					{
						($('.optiune-plata-combo')).val("Plata-rst");
					    $('.luna-plata-combo').attr("disabled", false);
					}
					else
					{
						($('.luna-plata-combo option:first')).attr('selected', 'selected');
						
							 if(val === 'L11') {
							 $(".camin-sum").val('300');
							
							} else if(val === 'L12') {
							 $(".camin-sum").val('200');
							} else {
								$( ".camin-sum" ).val("");	
								($('.default-selection')).show();
							}
						
					}
				
					setPopUpMessage("Finalizare tranzactie", "Plata caminului s-a realizat cu succes.");
					fonduri = Number(fonduri) - Number($( ".camin-sum" ).val());
					localStorage.setItem("fonduri", fonduri);
					
					$( ".nr-card-text" ).val("");
					$( ".cod-cw2-text" ).val("");
					$( ".nume-card-text" ).val("");
					
					//?? in functie de datele din combo
					
					$(".luna-combo").val($(".luna-combo option:first").val());
					$(".an-combo").val($(".an-combo option:first").val());
					
				} 
				}
			}else {
					setPopUpMessage("Esuat", "Verifica datele introduse.");
				}
		 }
		 
		 
		  // Pentru taxa maririlor sau a restantelor
		 if(window.location.href.includes('mariri_restante.html')) {
			 if(incercari===0)
			{
				 setPopUpMessage("Informatie", "Ti-ai consumat toate cele 4 mariri posibile!"); 
			}
			 else {
				 if(isValid) {
					 if(($('.disciplina-select')).val()!='NA') {
						var isRestMarireValid = validateSumForCaminOrRestanteMariri();
						if(isRestMarireValid) {									
							incercari = incercari - 1;
							
							if($('.marire-restante-select').val()=="marire")
								setPopUpMessage("Finalizare tranzactie", "Plata maririi s-a realizat cu succes."); 
							else {						
								setPopUpMessage("Finalizare tranzactie", "Plata restantei s-a realizat cu succes."); 
							}
										
							var disciplinaSelectata = ($('.disciplina-select')).val();
							console.log(disciplinaSelectata);
							console.log($(".disciplina-select option[value='"+ disciplinaSelectata + "']"));
							$(".disciplina-select option[value='"+ disciplinaSelectata + "']").remove();
						

							
							fonduri = parseInt(fonduri) - parseInt($( ".marire-restanta-sum" ).val());
							localStorage.setItem("fonduri", fonduri);
							localStorage.setItem("incercari_marire", incercari);
							
						 if(incercari >= 3) {
							 console.log(incercari);
							 $( ".marire-restanta-sum" ).val("0");
						 } else {
							 $( ".marire-restanta-sum" ).val("150");
						}
							
							
							$( ".nr-card-text" ).val("");
							$( ".cod-cw2-text" ).val("");
							$( ".nume-card-text" ).val("");	
							$(".luna-combo").prop("selectedIndex",0);
							$(".an-combo").prop("selectedIndex",0);
						}
					} else {
						setPopUpMessage("Informatii", "Nu ai materii pentru restanta.");
					}
				} 	else {
						setPopUpMessage("Esuat", "Verifica datele introduse.");
					}
			 }
				
		 }
			
	});	 
	

	 $(".optiune-plata-combo").change(function(){
		 var value =  $(".optiune-plata-combo").val();
		 if(value === 'Plata-rst') {
			 $('.luna-plata-combo').attr("disabled", false);
			 $(".ian").hide();
			 $('.luna-plata-combo').val("L11");
			 $(".camin-sum").val('300');
		 } else {
			 $('.luna-plata-combo').attr("disabled", true);
			 $('.luna-plata-combo').val("L1");
			 $(".camin-sum").val('155');
	
		 }
	 });
	 	 

	$(".luna-plata-combo").change(function(){
		var value =  $(".luna-plata-combo").val();
		 if(value === 'L11') {
			 $(".camin-sum").val('300');
			
		 } else if(value === 'L12') {
			 $(".camin-sum").val('200');
		 }
	});
	
	
	$('.marire-restante-select').change(function(){ 
		var value =  $(".marire-restante-select").val();
		if(value === 'restanta') {
			$(".disciplina-select option[value='NA']").show();
			$(".disciplina-select option[value='NA']").attr("selected","selected");
			$(".disciplina-select option[value='SD']").hide();
			$(".disciplina-select option[value='PIU']").hide();
			$(".disciplina-select option[value='PT']").hide();
			$(".disciplina-select option[value='CPD']").hide();
			$(".disciplina-select option[value='RC']").hide();
			$(".disciplina-select option[value='Marketing']").hide(); 
		} else {
			$(".disciplina-select option:first").attr("selected","selected");
			$(".disciplina-select option[value='SD']").show();
			$(".disciplina-select option[value='PIU']").show();
			$(".disciplina-select option[value='PT']").show();
			$(".disciplina-select option[value='CPD']").show();
			$(".disciplina-select option[value='RC']").show();
			$(".disciplina-select option[value='Marketing']").show();
			$(".disciplina-select option[value='NA']").hide();
		}
	});
	
});